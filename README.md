Input files for dumper CI tests
===============================

This repository houses inputs for the training dataset dumper CI tests.
For more information on the creation of test samples please take a look at the training dataset dumper [documentation](https://training-dataset-dumper.docs.cern.ch/test_files/).